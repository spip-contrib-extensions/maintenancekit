<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'maintenancekit_description' => 'Après installation, aller dans le menu Maintenance > Liste de Travaux pour lancer les fonctions dont vous avez besoin',
	'maintenancekit_nom' => 'Kit de maintenance',
	'maintenancekit_slogan' => 'Propose une série de fonctions pour nettoyer votre site',
);
